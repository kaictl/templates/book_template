# Custom Sites/Book/Fan-Fiction Repository

This repository is a perfect template for writing a book or fan-fiction, or just writing a normal website.

I personally like reading fictions that people have written on my ereader, not on a normal screen, so I tend to look far and wide for epub versions of whatever I'm interested in at the time.

Using this repo, you can generate a perfectly nice looking site, customize the flow and automatically build ebooks for people to download when you want to.

## Repo Instructions

It's relatively simple. Open up the `Book.code-workspace` file in Visual Studio Code and start modifying the `conf.py` file. I have clearly marked the parts you need to change near the top of the file.

## Gitlab Instructions

Create a new project and name it however you like. This will be used as the title of the book later on, so setting it to match the `project` value in `conf.py` is recommended.

Whenever code is pushed it'll update the url you have set in the [pages](/pages) settings.

You can use tags to generate ebooks and 'releases' which will automatically include links to the latest ebook version generated.

## And that's it!

Enjoy your writing!
