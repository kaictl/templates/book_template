# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

## EDIT THESE!
project = 'PROJECT TITLE'
author = 'YOUR NAME'
release = '0.1'
## Change the year here if you are using this in the future.
copyright = f'2023, {author}'

## Only edit if you're not writing in English.
language = 'en'

## ---------------------------------------------------------------------------
## You don't have to edit anything below this line
## But you can if you know what you're doing.
## ---------------------------------------------------------------------------
version = release

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = [
    'sphinx.ext.duration'
]

templates_path = ['_templates']
# I added the 'Ideas/bad' just so you could save your work but not get it
# included in the final page build.
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store', 'Ideas/bad']



# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = 'alabaster'
html_static_path = ['_static']
html_title = f'{project} {release}'
html_css_files = ['style/customs.css',]

# -- Options for epub output
suppress_warnings = [
    'epub.unknown_project_files'
]
## Use this to set a custom cover for the .epub version
# epub_cover = ('_static/cover.png', '')