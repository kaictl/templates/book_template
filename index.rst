.. Summoning Frontier documentation master file, created by
   sphinx-quickstart on Fri Jun 30 12:12:26 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

==============
 Series Title
==============

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   :glob:

   Book/*

.. image:: http://mirrors.creativecommons.org/presskit/buttons/80x15/svg/by-sa.svg
  :target: https://creativecommons.org/licenses/by-sa/4.0/